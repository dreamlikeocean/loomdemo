package com.dreamlike.loomdemo;

import org.apache.catalina.core.StandardThreadExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.util.Arrays;


@SpringBootApplication
@Import(LoomControllerBeanPostProcessor.class)
public class LoomdemoApplication implements CommandLineRunner {



	public static void main(String[] args) throws InterruptedException {
		final SpringApplication application = new SpringApplication(LoomdemoApplication.class);
		application.setWebApplicationType(WebApplicationType.SERVLET);
		application.run(args);
	}

	@Override
	public void run(String... args) throws Exception {


	}
}
