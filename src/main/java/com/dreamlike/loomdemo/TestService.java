package com.dreamlike.loomdemo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class TestService implements InitializingBean, BeanFactoryAware {
    @Autowired
    private BeanFactory beanFactory;

    public TestService() {
    }

    @Autowired
    public TestService(ApplicationContext applicationContext,Test test) {
        System.out.println("constructor");
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("init bean afterProperties");
    }

    @Autowired
    public void setA(ApplicationContext a){
        System.out.println("set application");
    }

    @PostConstruct
    public void a(){
        System.out.println("post conctruct");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("set bean factory");
    }


}
