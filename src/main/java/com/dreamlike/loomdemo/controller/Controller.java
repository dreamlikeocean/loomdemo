package com.dreamlike.loomdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class Controller {

    @RequestMapping("/word")
    public DeferredResult<String> word() throws InterruptedException {
        final DeferredResult<String> result = new DeferredResult<>();
        Thread.startVirtualThread(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result.setResult("string");
        });
        return result;
    }

    //http-nio-80-exec
    @RequestMapping("/word1")
    public String wor1d() throws InterruptedException {
        System.out.println(Thread.currentThread());
        return "ADw";
    }


}
