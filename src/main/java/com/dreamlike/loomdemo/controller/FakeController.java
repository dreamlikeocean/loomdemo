package com.dreamlike.loomdemo.controller;


import com.dreamlike.loomdemo.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FakeController {
    @Autowired
    private TestService testService;

    @GetMapping("/fake/{string}")
    public String fake(@PathVariable String string) throws InterruptedException {
        Thread.sleep(1000);
        System.out.println(testService == null);
        System.out.println(Thread.currentThread());
        return string;
    }
    private static class Wrapper{
        private Object target;

        public Object getTarget() {
            return target;
        }

        public void setTarget(Object target) {
            this.target = target;
        }

        public Wrapper(Object target) {
            this.target = target;
        }
    }
}
